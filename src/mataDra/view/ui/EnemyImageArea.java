package mataDra.view.ui;

import java.util.List;

import mataDra.logics.images.EnemyImageLogic;

public class EnemyImageArea {

	/**
	 * 敵キャラ文字シンボルを表示
	 *
	 * @param enemiesIndex
	 */
	public void show(List<Integer> groupIndex) {
		EnemyImageLogic enemyImageLogic = new EnemyImageLogic();
		String text="";
			text =enemyImageLogic.exec(groupIndex);
		System.out.println(text);


	}

}
