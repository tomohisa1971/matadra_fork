package mataDra.main.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import mataDra.view.ui.EnemyImageArea;

/**
 * 敵表示エリアに敵シンボルを表示するテスト
 *
 * @author ken4310
 *
 */
public class EnemyImageTest {
	public static void main(String[] args) {

		List<Integer> groupIndex = new ArrayList<>();
		// 敵モンスターのインデックス番号を格納
			int count = new Random().nextInt(5);
			for(int j = 0; j <count; j++){
				int index = new Random().nextInt(5);
				groupIndex.add(index);
			}
				EnemyImageArea enemyImageArea = new EnemyImageArea();
				enemyImageArea.show(groupIndex);



	}
}
