package mataDra.entity.creatures;

import java.util.List;

import mataDra.entity.ability.AbilityEntity;

public class PlayerEntity extends CreatureEntity {

	private final int MAX_LEVEL;//最大レベル
	private boolean isLeader;//リーダー判定
	private int rarity;//レア度






	/**コンストラクタ自動生成
	 * @param index
	 * @param name
	 * @param hp
	 * @param charge
	 * @param attack
	 * @param experience
	 * @param level
	 * @param abilities
	 * @param battleStateType
	 * @param attribute
	 * @param mAX_LEVEL
	 * @param isLeader
	 * @param rarity
	 */
	public PlayerEntity(int index, String name, int hp, int charge, int attack, int experience, int level,
			List<AbilityEntity> abilities, BattleStateType battleStateType, Attribute attribute, int mAX_LEVEL,
			boolean isLeader, int rarity) {
		super(index, name, hp, charge, attack, experience, level, abilities, battleStateType, attribute);
		MAX_LEVEL = mAX_LEVEL;
		this.isLeader = isLeader;
		this.rarity = rarity;
	}



	//ゲッターセッター自動生成


	public boolean isLeader() {
		return isLeader;
	}



	public void setLeader(boolean isLeader) {
		this.isLeader = isLeader;
	}



	public int getRarity() {
		return rarity;
	}



	public void setRarity(int rarity) {
		this.rarity = rarity;
	}



	public int getMAX_LEVEL() {
		return MAX_LEVEL;
	}


//ToString自動生成
	@Override
	public String toString() {
		return "PlayerEntity [MAX_LEVEL=" + MAX_LEVEL + ", isLeader=" + isLeader + ", rarity=" + rarity
				+ ", isLeader()=" + isLeader() + ", getRarity()=" + getRarity() + ", getMAX_LEVEL()=" + getMAX_LEVEL()
				+ ", getName()=" + getName() + ", getHp()=" + getHp() + ", getCharge()=" + getCharge()
				+ ", getAttack()=" + getAttack() + ", getExperience()=" + getExperience() + ", getLevel()=" + getLevel()
				+ ", getAbilities()=" + getAbilities() + ", getBattleStateType()=" + getBattleStateType()
				+ ", getAttribute()=" + getAttribute() + ", getIndex()=" + getIndex() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}













}
