package mataDra.entity.ability;

import mataDra.entity.Entity;

public class AbilityEntity extends Entity{

	public enum AbilityType{
		DOUBLING,ATTACK,RECOVER
	}

    private String name;// 名前
    private String desc;// 説明
    private String msg;// 使用時メッセージ
    private int point;// 攻撃力・回復力
    private int imageIndex;// 画像No
    private int soundIndex;// サウンドNo
    private int charge;// チャージターン数
    private AbilityType abilityType;



	/**コンストラクタ自動生成
	 * @param index
	 * @param name
	 * @param desc
	 * @param msg
	 * @param point
	 * @param imageIndex
	 * @param soundIndex
	 * @param charge
	 * @param abilityType
	 */
	public AbilityEntity(int index, String name, String desc, String msg, int point, int imageIndex, int soundIndex,
			int charge, AbilityType abilityType) {
		super(index);
		this.name = name;
		this.desc = desc;
		this.msg = msg;
		this.point = point;
		this.imageIndex = imageIndex;
		this.soundIndex = soundIndex;
		this.charge = charge;
		this.abilityType = abilityType;
	}


	//ゲッターセッター自動生成
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getDesc() {
		return desc;
	}



	public void setDesc(String desc) {
		this.desc = desc;
	}



	public String getMsg() {
		return msg;
	}



	public void setMsg(String msg) {
		this.msg = msg;
	}



	public int getPoint() {
		return point;
	}



	public void setPoint(int point) {
		this.point = point;
	}



	public int getImageIndex() {
		return imageIndex;
	}



	public void setImageIndex(int imageIndex) {
		this.imageIndex = imageIndex;
	}



	public int getSoundIndex() {
		return soundIndex;
	}



	public void setSoundIndex(int soundIndex) {
		this.soundIndex = soundIndex;
	}



	public int getCharge() {
		return charge;
	}



	public void setCharge(int charge) {
		this.charge = charge;
	}



	public AbilityType getAbilityType() {
		return abilityType;
	}



	public void setAbilityType(AbilityType abilityType) {
		this.abilityType = abilityType;
	}


	//ToString自動生成
	@Override
	public String toString() {
		return "AbilityEntity [name=" + name + ", desc=" + desc + ", msg=" + msg + ", point=" + point + ", imageIndex="
				+ imageIndex + ", soundIndex=" + soundIndex + ", charge=" + charge + ", abilityType=" + abilityType
				+ "]";
	}





}
