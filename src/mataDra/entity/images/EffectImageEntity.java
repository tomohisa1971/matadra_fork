package mataDra.entity.images;

public class EffectImageEntity extends ImageEntity {
    public enum EffectType{
    	SINGLE,MULTI
    }
	private String image;


	/**コンストラクタ自動生成
	 *
	 */
	private EffectType effectType;
	public EffectImageEntity(int index, String name, String image, EffectType effectType) {
		super(index, name);
		this.image = image;
		this.effectType = effectType;
	}

	//ゲッターセッター自動生成
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public EffectType getEffectType() {
		return effectType;
	}
	public void setEffectType(EffectType effectType) {
		this.effectType = effectType;
	}

	//ToString自動生成
	@Override
	public String toString() {
		return "EffectImageEntity [image=" + image + ", effectType=" + effectType + ", getImage()=" + getImage()
				+ ", getEffectType()=" + getEffectType() + ", getName()=" + getName() + ", getIndex()=" + getIndex()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}






}
