package mataDra.entity.images;

import mataDra.entity.Entity;

public abstract class ImageEntity extends Entity {

	private String name;

	/**
	 * コンストラクタ自動作成
	 *
	 * @param index
	 * @param name
	 */
	public ImageEntity(int index, String name) {
		super(index);
		this.name = name;
	}

	// ゲッターセッター自動生成
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}