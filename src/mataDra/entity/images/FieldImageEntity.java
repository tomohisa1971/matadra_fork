package mataDra.entity.images;

public class FieldImageEntity extends ImageEntity {
    private String Image;

    /**コンストラクタ自動作成
     * @param index
     * @param name
     * @param image
     */
    public FieldImageEntity(int index, String name, String image) {
        super(index, name);
        Image = image;
    }
    //ゲッターセッター自動生成
    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }
    //toString自動生成
    @Override
    public String toString() {
        return "FieldImage [Image=" + Image + ", getImage()=" + getImage() + ", getName()=" + getName()
                + ", getIndex()=" + getIndex() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
                + ", toString()=" + super.toString() + "]";
    }

}
