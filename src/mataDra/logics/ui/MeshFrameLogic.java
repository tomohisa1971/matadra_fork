package mataDra.logics.ui;

/**
 * メッセージ外枠表示クラス
 *
 *
 */
public class MeshFrameLogic extends FrameLogic {






	// コンストラクタ
    public MeshFrameLogic() {
        // 初期設定は全角＃
        setBorder("＃");
    }


    /**
     * メッセージ外枠(上下部)の作成
     *
     */
    public void drawUpBorder(String text) {
        // 外枠最大サイズ
        int frameWidth = text.length() + 4;

        // 外枠を作成
        for (int i = 0; i < frameWidth; i++) {
            System.out.print(getBorder());
        }
        // 改行
        System.out.print("\n");
    }


	public void drawBottomBorder(String text) {
		drawUpBorder(text);

	}


    /**
     * 外枠左線の作成
     *
     * @param text
     */
    public void drawLeftBorder() {

        // メッセージ外枠左側の表示
        System.out.print(getBorder());
        // メッセージ余白(全角スペース1つ)
        System.out.print("　");

    }

    /**
     * 外枠右線の作成
     *
     * @param text
     */
    public void drawRightBorder() {

        // メッセージ余白(全角スペース1つ)
        System.out.print("　");
        // メッセージ外枠右側の表示
        System.out.print(getBorder());
        System.out.print("\n");
    }




}
