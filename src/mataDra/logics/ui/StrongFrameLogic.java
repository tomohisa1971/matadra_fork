package mataDra.logics.ui;

/**
 * メッセージ外枠表示クラス
 *
 *
 */
public class StrongFrameLogic extends FrameLogic {
    // 外枠の文字数



    // コンストラクタ
    public StrongFrameLogic() {
        // 初期設定は全角＃
        setBorder("人");
        setBorderBottom("Y^");
    }



    /**
     * メッセージ外枠(上下部)の作成
     *
     */
	@Override
	public void drawUpBorder(String text) {
		// 外枠を作成
		System.out.print("＿");
        for (int i = 0; i < text.length(); i++) {
            System.out.print(getBorder());
        }
        // 改行
        System.out.print("＿\n");

	}

	@Override
	public void drawBottomBorder(String text) {
		// 外枠を作成

		System.out.print("￣");
        for (int i = 0; i < text.length(); i++) {
            System.out.print(getBorderBottom());
        }
        // 改行
        System.out.print("￣\n");
	}

    /**
     * 外枠左線の作成
     *
     * @param text
     */
    public void drawLeftBorder() {

        // メッセージ外枠左側の表示
        System.out.print('＞');

    }

    /**
     * 外枠右線の作成
     *
     * @param text
     */
    public void drawRightBorder() {

        // メッセージ外枠右側の表示
        System.out.print('＜');
        System.out.print("\n");
    }



}
