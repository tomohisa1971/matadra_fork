package mataDra.logics.items;

import mataDra.dao.items.AttackItemDAO;
import mataDra.entity.items.consume.AttackItemEntity;

public class AttackItemListLogic extends ItemListLogic<AttackItemEntity> {

	@Override
	public AttackItemEntity getItem(int index) {
		// TODO 自動生成されたメソッド・スタブ
		AttackItemDAO dao = new AttackItemDAO();
		return dao.findByIndex(index);
	}

}
