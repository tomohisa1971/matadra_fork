package mataDra.logics.images;

import mataDra.dao.images.EffectDAO;
import mataDra.logics.DAOLogic;

public class EffectImageLogic extends DAOLogic<EffectDAO> {

	// コンストラクタ
	public EffectImageLogic() {
		// 登録したデータを読み込む
		setDao(new EffectDAO());
		// データの数を記録する
		setCount(getDao().registerAll().size());

	}

	// 表示するオブジェクトを返す
	public String convert(int index) {
		// TODO 自動生成されたメソッド・スタブ

		return getDao().findByIndex(index).getImage();
	}

	// インデックスが正しければオブジェクトをカウントの数だけ表示する
	// (全体エフェクト)
	public String exec(int index, int count) {
		String text = "";
		for (int i = 0; i < count; i++) {
			text += checkIndex(index);

		}
		text += "\n";
		return text;
	}

	// インデックスが正しければオブジェクトをターゲットの位置に表示する
	// (単体エフェクト)
	public String exec(int index, int count, int target) {
		String text = "";
		for (int i = 0; i < count; i++) {
			if(target == i){
				text += checkIndex(index);
				continue;
			}
			text+="　";


		}
		text += "\n";
		return text;
	}

}
